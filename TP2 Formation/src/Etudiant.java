import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Etudiant {

	private HashMap<String,ArrayList<Integer>> resultats;
	private Identite identite;
	private Formation formation;
	
	@SuppressWarnings("rawtypes")
	public Etudiant(Identite identite, Formation formation) {
		super();
		
		this.identite = identite;
		this.formation = formation;
		this.resultats= new HashMap<String,ArrayList<Integer>>();
		HashMap<String, Double> m=this.formation.getMatieres();
		Iterator i=m.keySet().iterator();
		while(i.hasNext()) {
			Object key=i.next();
			resultats.put((String)key,new ArrayList<Integer>());
		}
	}
	
	public void ajouternote(String matiere,int note) {
		if(note>=0 && note<=20 ) {
			if(resultats.containsKey(matiere)) {
				ArrayList<Integer> notes=resultats.get(matiere);
				notes.add(note);
				resultats.replace(matiere, notes);
			}
		}
	}
	
	public double calculerMoyenneMatiere(String matiere) throws Exception {
		if(resultats.containsKey(matiere)) {
			ArrayList<Integer> notes=resultats.get(matiere);
			int i=0;
			int moy=0;
			
			while (i<notes.size()) {
				moy=moy+notes.get(i);
				i++;
			}
			moy=moy/notes.size();
			return moy;
		}else{
			throw new Exception("Matiere non existante");
		}
	}
	
	@SuppressWarnings("rawtypes")
	public double calculerMoyenneGeneral() throws Exception {

		double moygenerale=0;
		int n=0;
		Iterator i=this.formation.getMatieres().keySet().iterator();
		while(i.hasNext()) {
			Object key=i.next();
			System.out.println(key);
			double moy=calculerMoyenneMatiere((String) key);
			double notematiere=moy*this.formation.getMatieres().get(key);
			moygenerale=moygenerale+notematiere;
			n++;
			System.out.println(n);
		}
		return moygenerale/n;
		
	}



	public HashMap<String, ArrayList<Integer>> getResultats() {
		return resultats;
	}

	public Identite getIdentite() {
		return identite;
	}

	public Formation getFormation() {
		return formation;
	}
	
	
}
