import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class EtudiantTest {
		
	private Formation formation;
    private Etudiant etudiant;
    private Identite ident;
			

			@Before
			public void init() {
				formation=new Formation("Informatique");
				formation.ajouterMatiere("Math", 0.2);
				formation.ajouterMatiere("Info", 0.3);
				formation.ajouterMatiere("Anglais", 0.1);
				ident=new Identite("0001", "Jean", "Jacques");
					System.out.println(formation.getMatieres());
				etudiant=new Etudiant(ident, formation);
			}

	@Test
	public void testAjouternote() {
		etudiant.ajouternote("Info", 13);
		assertEquals(13,0,(double)(etudiant.getResultats().get("Info").get(0)));
	}

	@Test
	public void testCalculerMoyenneMatiere() throws Exception {
		etudiant.ajouternote("Anglais", 10);
		etudiant.ajouternote("Anglais", 20);
		assertEquals(etudiant.calculerMoyenneMatiere("Anglais"),15,0);
	}

	@Test
	public void testCalculerMoyenneGeneral() throws Exception {
		etudiant.ajouternote("Anglais", 10);
		etudiant.ajouternote("Anglais", 20);
		etudiant.ajouternote("Math", 0);
		etudiant.ajouternote("Info", 0);
		assertEquals(etudiant.calculerMoyenneGeneral(),0.5,0);
	}

}
