import java.util.HashMap;

public class Formation {
	
	private String identifiant;
	private HashMap<String,Double> matieres;
	
	public Formation(String identifiant) {
		super();
		this.identifiant = identifiant;
		this.matieres = new HashMap<String,Double>();
	}
	
	public void ajouterMatiere(String matiere,double coeff) {
		if(!(matieres.containsKey(matiere))) {
			matieres.put(matiere, coeff);
		}
	}
	
	public void SupprimerMatiere(String matiere) {
		if((matieres.containsKey(matiere))) {
			matieres.remove(matiere);
		}
	}

	public String getIdentifiant() {
		return identifiant;
	}
	
	public double getCoefficient(String matiere) throws Exception {
		if(matieres.containsKey(matiere)) {
			return(matieres.get(matiere));
		}
		throw new Exception("Matiere non existante");
		
	}

	public HashMap<String, Double> getMatieres() {
		return matieres;
	}

	
	
	
	
	
}
