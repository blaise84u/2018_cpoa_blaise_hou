import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class FormationTest {
	private Formation formation;

		

		@Before
		public void init() {
			formation=new Formation("Informatique");
		
			
			
		}

	@Test
	public void testAjouterMatiere() {
		formation.ajouterMatiere("Math", 2);
		assertEquals(2,formation.getMatieres().get("Math"),0);
	}

	@Test
	public void testSupprimerMatiere() {
		formation.SupprimerMatiere("Math");
		assertTrue("La r�ponse devrait etre vrai",formation.getMatieres().isEmpty());
		
	}

	@Test
	public void testGetCoefficient() throws Exception {
		formation.ajouterMatiere("Math", 2);
		assertEquals(2,formation.getCoefficient("Math"),0);
	}
	@Test(expected = Exception.class)
	public void coefficientWithException() throws Exception{
		formation.getCoefficient("Anglais");
	}
	

}
