import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;

import exercice2.AdresseIP;

public class Groupe {
	
	private Formation formation1;
	private ArrayList<Etudiant> etudiants;
	
	public Groupe(Formation formation1) {
		super();
		this.formation1 = formation1;
		this.etudiants = new ArrayList<Etudiant>();
	}
	public void ajouterEtudiant(Etudiant etu) {
		if(this.formation1.equals(etu.getFormation())) {
			this.etudiants.add(etu);
		}
	}
	public void supprimerEtudiant(Etudiant etu) {
			if(this.etudiants.contains(etu)) {
				this.etudiants.remove(etu);
			}
	}
	public Formation getFormation1() {
		return formation1;
	}

	public ArrayList<Etudiant> getEtudiants() {
		return etudiants;
	}
	
	public double calculerMoyenneGeneral() throws Exception {
		double moy=0;
		int n=0;
		Iterator i=this.formation1.getMatieres().keySet().iterator();
		while(i.hasNext()) {
			Object key=i.next();
			moy=moy+calculerMoyenneMatiere((String) key);
			n++;
		}
		return moy=moy/n;
	}
	
	public double calculerMoyenneMatiere(String matiere) throws Exception {
		double notes=0;
		double moy=0;
		if(this.formation1.getMatieres().containsKey(matiere)) {
			for(int i=0;i<this.etudiants.size();i++) {
				notes=notes+etudiants.get(i).calculerMoyenneMatiere(matiere);
				
			}
			moy=notes/etudiants.size();
			return moy;
		}else {
			throw new Exception("Matiere non existante");
		}
		
	}
	public void triParMerite() throws Exception {
		for (int j = 0; j < etudiants.size() - 1; j++) {
			for (int m = 1; m < etudiants.size() - j; m++) {
				Etudiant a;
				BigDecimal val1=new BigDecimal(etudiants.get(m - 1).calculerMoyenneGeneral());
				BigDecimal val2=new BigDecimal(etudiants.get(m).calculerMoyenneGeneral());
				if (val1.compareTo(val2)>0){
					a = etudiants.get(m - 1);
					etudiants.set((m - 1), etudiants.get(m));
					etudiants.set(m, a);
				}
			}
		}
	}
	public void triAlpha() {
		for (int j = 0; j <etudiants.size() - 1; j++) {
			for (int m = 1; m < etudiants.size() - j; m++) {
				Etudiant a;
				if ()) {
					a = etudiants.get(m - 1);
					etudiants.set((m - 1), etudiants.get(m));
					etudiants.set(m, a);
				}
			}
		}
	}
	
	
}
