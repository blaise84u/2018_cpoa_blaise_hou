import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class Annuaire<K,V> extends HashMap<K,V>{
	
	public Annuaire () {
		super();
	}
	public void domaine() {
		Iterator i=this.keySet().iterator();
		while(i.hasNext()) {
			Object key=i.next();
			System.out.println(key);
		}
	}
	public String acces(String nom) {
		boolean contient=this.containsKey(nom);
		if(contient) {
			return (String) this.get(nom);
		}else {
			return null;
		}
	}
	public void adjonction(String nom,String num) {
		if(!this.containsKey(nom)) {
			this.put((K)nom, (V)num);
		}
		
	}
	public void suppresion(String nom) {
		if(this.containsKey(nom)) {
				this.remove(nom);
		}
		
	}
	public void changement(String nom,String num) {
		if(this.containsKey(nom)) {
			this.remove(nom);
			this.put((K)nom, (V)num);
	}
	}
}
