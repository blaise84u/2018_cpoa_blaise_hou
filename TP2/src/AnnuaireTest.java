import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AnnuaireTest {
	Annuaire a;
	String nom1;
	String nom2;
	
	String num1;
	String num2;
	@Before
	public void init() {
		a=new Annuaire();
		nom1="jacque";
		nom1="jean";
				
		num1="0600000000";
		
		num2="0612345670";
	}

	@Test
	public void testAcces() {
		a=new Annuaire();
		a.adjonction("jacque","0600000000");
		this.a.domaine();
		assertEquals("le num doit etre accesible",num1,this.a.acces("jacque"));
	}

	@Test
	public void testAdjonction() {
		a.adjonction(nom2, num2);
		assertEquals("le num doit etre accesible",num2,this.a.acces(nom2));
	}

	@Test
	public void testSuppresion() {
		a.adjonction(nom1,num1);
		a.adjonction(nom2, num2);
		this.a.suppresion(nom2);
		
		assertEquals("le num doit etre supprime",null,this.a.acces(nom2));
	}

	@Test
	public void testChangement() {
		a.adjonction(nom1,num1);
		a.adjonction(nom2, num2);
		this.a.changement(nom2, num1);
		assertEquals("le num doit etre change",num1,this.a.acces(nom2));
	}

}
