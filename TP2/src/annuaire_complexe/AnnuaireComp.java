package annuaire_complexe;

import java.util.HashMap;
import java.util.Iterator;

public class AnnuaireComp extends HashMap<Personne,String> {

	public AnnuaireComp() {
		super();
	}
	
	
	public void domaine() {
		Iterator i=this.keySet().iterator();
		while(i.hasNext()) {
			Object key=i.next();
			System.out.println(this.get(key));
		}
		
	}
	
	public String acces(String nom,String prenom) {
		Iterator i=this.keySet().iterator();

		while(i.hasNext()) {
			Object key=i.next();
			if( ((Personne)key).getNom()==nom &&((Personne)key).getPrenom()==prenom ) {
				return this.get(key);
			}
		}
		return null;
		
		
	}
		
	
	public void adjonction(String nom,String prenom,String num) {
		Personne p=new Personne(nom,prenom);
		if(!(this.containsKey(p))) {
			this.put(p, num);
		}
	}
	
	public void suppression(String nom,String prenom) {
		Iterator i=this.keySet().iterator();
		while(i.hasNext()) {
			Object key=i.next();
			if( ((Personne)key).getNom()==nom &&((Personne)key).getPrenom()==prenom ) {
				this.remove(key);
			}
		}
		
	}
	
	public void changement(String nom,String prenom,String num)  {
		Iterator i=this.keySet().iterator();
		while(i.hasNext()) {
			Object key=i.next();
			if( ((Personne)key).getNom()==nom &&((Personne)key).getPrenom()==prenom ) {
				this.replace((Personne) key,num);
			}
		}
		
	}
	
	
}
