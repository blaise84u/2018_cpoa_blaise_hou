package annuaire_complexe;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AnnuaireCompTest {
	Personne p1;
	Personne p2;
	String num1;
	String num2;
	AnnuaireComp ac;

	@Before
	public void init() {
		p1 = new Personne("Jean", "Dupont");
		p2 = new Personne("Jacques", "Dupuis");

		num1 = "0600000000" ;

		num2 = "0612345670" ;

		ac = new AnnuaireComp();
	}

	@Test
	public void testAcces() {
		ac.adjonction("Jean", "Dupont", num1);
		this.ac.domaine();
		assertEquals("le num doit etre accesible", num1, this.ac.acces("Jean", "Dupont"));
	}

	@Test
	public void testAdjonction() {
		ac.adjonction(p2.getNom(),p2.getPrenom(), num1);
		assertEquals("le num doit etre accesible", num1, this.ac.acces(p2.getNom(),p2.getPrenom()));
	}

	@Test
	public void testSuppresion() {
		ac.adjonction(p1.getNom(),p1.getPrenom(), num1);
		ac.adjonction(p2.getNom(),p2.getPrenom(), num2);
		this.ac.suppression(p1.getNom(),p1.getPrenom());
		assertEquals("le num doit etre supprime", null, this.ac.acces(p1.getNom(),p1.getPrenom()));
	}

	@Test
	public void testChangement() {
		ac.adjonction(p2.getNom(),p2.getPrenom(), num2);
		this.ac.changement(p2.getNom(),p2.getPrenom(), num1);
		assertEquals("le num doit etre change", num1, this.ac.acces(p2.getNom(),p2.getPrenom()));
	}
}
