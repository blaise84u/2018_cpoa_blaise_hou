package exercice2;

public class AdresseIP {

	private int[] adr;

	public AdresseIP() {
		this.adr = new int[4];
		for(int i=0;i<4;i++) {
			adr[i]=0;
		}
	}
	public int[] getint() {
		return this.adr;
	}
	public boolean comparer(AdresseIP p) {
		int[] res=p.getint();
		for(int i=0;i<4;i++) {
			if(adr[i]<res[i]) {
				return true;
			}else {
				if(adr[i]>res[i]) {
					return false;
				}
			}
		}
		return false;
	}
	public boolean unicite(AdresseIP p) {
		int[] res=p.getint();
		for(int i=0;i<4;i++) {
			if(adr[i]==res[i]) {
				return true;
			}
		}
		return false;
	}
	public void add(int m,int n) {
		adr[n]=adr[n]*10+m;
	}
	public String toString() {
		String s=""+adr[0];
		for(int i=1;i<4;i++) {
			s=s+"."+adr[i];
		}
		return s;
	}
}
