package exercice2;

import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ListeIp {

	HashSet<AdresseIP> ips;
	boolean trier;

	public ListeIp(boolean t) {
		ips = new HashSet<AdresseIP>();
		this.trier = t;
	}

	public void chargerFichier(String name) throws IOException {
		InputStream flux = new FileInputStream(name);
		InputStreamReader lecture = new InputStreamReader(flux);
		BufferedReader buff = new BufferedReader(lecture);
		String ligne;
		List<AdresseIP> temp = new ArrayList<AdresseIP>();
		while ((ligne = buff.readLine()) != null) {
			String[] res = ligne.split("log\\S");
			String s = res[0];
			char[] c = s.toCharArray();
			int n = 0;
			AdresseIP p = new AdresseIP();
			for (int i = 0; i < c.length; i++) {

				if (c[i] != '.' && !String.valueOf(c[i]).equals(" ")) {
					p.add(Integer.parseInt(String.valueOf(c[i])), n);
				} else {
					n++;
				}

			}
			boolean unique=true;
			Iterator i = ips.iterator();
			while (i.hasNext()&&unique) {
				Object key = i.next();
				unique=!((AdresseIP)key).unicite(p);
			}
			if(unique) {
			ips.add(p);
			}
		}

		buff.close();
	}

	public String toString() {
		Iterator i = ips.iterator();
		String s = "";
		if (this.trier) {
			ArrayList<AdresseIP> temp = new ArrayList<AdresseIP>();
			while (i.hasNext()) {
				Object key = i.next();
				temp.add((AdresseIP) key);
			}
			for (int j = 0; j < temp.size() - 1; j++) {
				for (int m = 1; m < temp.size() - j; m++) {
					AdresseIP a;
					if (!temp.get(m - 1).comparer(temp.get(m))) {
						a = temp.get(m - 1);
						temp.set((m - 1), temp.get(m));
						temp.set(m, a);
					}
				}
			}
			for(AdresseIP a:temp) {
				s=s+a.toString()+"\n";
			}
		} else {
			while (i.hasNext()) {
				Object key = i.next();
				s = s + key.toString() + "\n";
			}
		}
		return s;

	}

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("si vous voulez que le resultat est trie plz tapez 'true' :");
		boolean tri;
		if(sc.next().equals("true")) {
			tri=true;
		}else {
			tri=false;
		}
		ListeIp i = new ListeIp(tri);
		try {
			i.chargerFichier(
					"C:/Users/machilus.DESKTOP-MACHILU/Desktop/iut-nancy info s3/2018_cpoa_blaise_hou/TP2/src/exercice2/logs.txt");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(i.toString());
	}

}
