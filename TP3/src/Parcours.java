import java.util.Iterator;

public abstract class Parcours implements Iterator<Integer> {
	
	protected TableauEntier tb;
	protected int ligneCour;
	protected int colonneCour;
	private int nbParcourus;

	public Parcours(TableauEntier t) {
		super();
		this.tb=t;
		this.ligneCour=0;
		this.colonneCour=0;
		
	}


	public abstract void suivant();

	public Integer next() {
		int entier=tb.valeurA(ligneCour, colonneCour);
		this.suivant();
		return entier;
	}


	
}
