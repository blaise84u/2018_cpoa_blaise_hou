
public class ParcoursLigne extends Parcours{
	
	public ParcoursLigne(TableauEntier t) {
		super(t);
	}
	



	@Override
	public void suivant() {
		this.colonneCour++;
	}




	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		if(this.colonneCour>=this.tb.getLargueur()) {
			this.ligneCour=this.ligneCour+1;
			this.colonneCour=0;
		}
		if((this.ligneCour)>=this.tb.getHauteur()){
			return false;
		}else {
			return true;
		}
	}
	
	


}
