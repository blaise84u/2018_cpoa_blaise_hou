
public class ParcoursZigzag extends Parcours {

	public ParcoursZigzag(TableauEntier t) {
		super(t);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void suivant() {
		if(this.ligneCour%2==0) {
		this.colonneCour++;
		}else {
			this.colonneCour--;
		}
	}
	@Override
	public boolean hasNext() {
		// TODO Auto-generated method stub
		if(this.colonneCour>=this.tb.getLargueur()) {
			this.ligneCour=this.ligneCour+1;
			this.colonneCour--;
		}
		if(this.colonneCour<=-1) {
			this.ligneCour=this.ligneCour+1;
			this.colonneCour++;
		}
		if((this.ligneCour)>=this.tb.getHauteur()){
			return false;
		}else {
			return true;
		}
	}
}
