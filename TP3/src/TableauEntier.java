import java.util.Iterator;

public abstract class TableauEntier {

private int[][] tableau;

	public TableauEntier(int[][] t) {
		tableau=t;
	}
	
	public int valeurA(int l, int c) {
		return tableau[l][c];
	}
	
	public int getLargueur() {
		return tableau[0].length;
		
	}
	
	public int getHauteur() {
		return tableau.length;
	}
	public ParcoursLigne iterateurLigne() {
		return (new ParcoursLigne(this));
	}
	public ParcoursZigzag iterateurZigzag() {
		return (new ParcoursZigzag(this));
	}
	public ParcoursColonne iterateurColonne() {
		return (new ParcoursColonne(this));
	}
	

}
