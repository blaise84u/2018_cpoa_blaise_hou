import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.*;

import org.junit.Test;

public class TableauEntierTest {

	@Test
	public void testIterateurLigne() {
		int[][] t= {{1,2,3},{4,5,6},{7,8,9},{10,11,12}};
		TableauEntier  t1=new TableauEntier(t);
		
		Iterator<Integer> it=new ParcoursLigne(t1);
		int[] l1={1,2,3,4,5,6,7,8,9,10,11,12};
		int[] l2=new int[12];
		int i=0;
		it=t1.iterateurLigne();
		while(it.hasNext()) {
			l2[i]=it.next();
			i++;
		}

		assertEquals("",l1[0],l2[0]);
	}
	@Test
	public void testIterateurZigzag() {
		int[][] t= {{1,2,3},{4,5,6},{7,8,9},{10,11,12}};
		TableauEntier  t1=new TableauEntier(t);
		
		Iterator<Integer> it=new ParcoursZigzag(t1);
		int[] l1={1,2,3,6,5,4,7,8,9,12,11,10};
		int[] l2=new int[12];
		int i=0;
		it=t1.iterateurZigzag();
		while(it.hasNext()) {
			l2[i]=it.next();
			i++;
		}

		assertEquals("",l1[0],l2[0]);
	}
	@Test
	public void testIterateurColonne() {
		int[][] t= {{1,2,3},{4,5,6},{7,8,9},{10,11,12}};
		TableauEntier  t1=new TableauEntier(t);
		
		Iterator<Integer> it=new ParcoursColonne(t1);
		int[] l1={1,2,3,6,5,4,7,8,9,12,11,10};
		int[] l2=new int[12];
		int i=0;
		it=t1.iterateurColonne();
		while(it.hasNext()) {
			l2[i]=it.next();
			System.out.println(l2[i]);
			i++;
		}

		assertEquals("",l1[0],l2[0]);
	}
	
}
