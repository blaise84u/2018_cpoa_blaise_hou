
public class Smiley extends DecorateurRene {


	
	public Smiley(ReneLaTaupe c) {
		super(c);
		this.nomIm="img/Smiley.png";
	}

	@Override
	public void afficher() {
		i=accessoires.i;
        i.paintOver("img/Smiley.png", 260,210);
        i.display();  
        
	}
}
