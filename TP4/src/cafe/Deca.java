package cafe;

/**
 * Classe correspondant a une boisson Colombia
 * 
 */
public class Deca extends Boisson {
	
	public Deca(){
		description = " Deca";
	}
	
	/**
	 * @return prix de la boisson
	 */
	public double cout() {
		return 1;
	}

}
