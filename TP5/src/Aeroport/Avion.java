package Aeroport;

public class Avion extends Thread {
	private String nom;
	private Aeroport a;

	public Avion(String s) {
		this.nom = s;
	}

	public void run() {
		
		this.a = a.getInstance();
		System.out.println("Je suis avion" + this.nom + " sur aeroport " + this.a );
		boolean takeoff = false;
		while (!takeoff) {
			if (this.a.autoriserAdecoller()) {
				System.out.println(this.nom + " d�colle");
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				this.a.liberer_piste();
				takeoff=true;

			} else {
				System.out.println(this.nom +" en attente");
				try {
					Thread.sleep((long) (Math.random()*100));
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
			
		}
		System.out.println(this.nom + " a d�coll�");
	}
}