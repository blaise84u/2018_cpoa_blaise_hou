

public class FabriqueIntersection implements FabriqueVehicule {

	private double probaVoiture;
	private double probaBus;
	private double probaByc;
	private double probaPieton;
	
	public FabriqueIntersection() 
	{
		this.probaVoiture=0.80;
		this.probaBus=0.05;
		this.probaByc=0.05;
		this.probaPieton=0.10;		
	}
	
	public FabriqueIntersection(double pv,double pb,double pby,double pp) 
	{
		this.probaVoiture=pv;
		this.probaBus=pb;
		this.probaByc=pby;
		this.probaPieton=pp;
		
	}
	@Override
	public Vehicule creerVehicule() {
		double r=Math.random();
		if(r<=0.80) {
			return new Voiture();
		}
		if(r<=0.90 && r>0.80) {
			return new Pieton();
		}
		if(r<=0.95 && r>0.90) {
			return new Bus();
		}
		if(r<=1 && r>0.95) {
			return new Bicyclette();
		}
		return null;	
	}
}
