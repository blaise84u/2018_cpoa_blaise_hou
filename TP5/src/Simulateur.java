import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Simulateur {
	private FabriqueVehicule fab;

	public Simulateur(FabriqueVehicule f) {
		this.fab = f;
	}

	public HashMap genererStats() {

		HashMap<Integer, Vehicule> hash = new HashMap<Integer, Vehicule>();
		List<Integer> number = new ArrayList<Integer>();
		int i = 0;
		while (i < 100) {
			number.add(i);
			i++;
		}
		Iterator<Integer> it = number.iterator();
		while (it.hasNext()) {
			hash.put(it.next(), fab.creerVehicule());
		}
		return hash;
	}

	public void ecrireStats() {
		HashMap<Integer, Vehicule> hash = this.genererStats();
		Iterator it = hash.keySet().iterator();
		int voiture = 0;
		int bus = 0;
		int bicyclette = 0;
		int pieton = 0;

		while (it.hasNext()) {
			int i = (int) it.next();
			Vehicule v = hash.get(i);
			switch (v.getType()) {
			case "Voiture":
				voiture++;
				break;
			case "Bus":
				bus++;
				break;
			case "Bicyclette":
				bicyclette++;
				break;
			case "Pieton":
				pieton++;
				break;
			}
		}
		System.out.println("Voiture : " + voiture);
		System.out.println("Bus : " + bus);
		System.out.println("Bicyclette : " + bicyclette);
		System.out.println("Pieton : " + pieton);
	}

	public void dessinerStats()
	{
		HashMap<Integer, Vehicule> hash = this.genererStats();
		JFrame fenetre = new JFrame(" Stats ");
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel dessin=new JPanel() {
			public void paintComponent ( Graphics g ) 
			{
				super.paintComponent( g );
				
				Iterator it = hash.keySet().iterator();
				int voiture = 0;
				int bus = 0;
				int bicyclette = 0;
				int pieton = 0;

				while (it.hasNext()) {
					int i = (int) it.next();
					Vehicule v = hash.get(i);
					switch (v.getType()) {
					case "Voiture":
						voiture++;
						break;
					case "Bus":
						bus++;
						break;
					case "Bicyclette":
						bicyclette++;
						break;
					case "Pieton":
						pieton++;
						break;
					}
				}
				Font font=new Font("Arial Narrow",Font.BOLD,10);
				
				g.setColor(Color.RED);
				g.drawString("Voiture",100,150);
				/*g.drawRect(100*voiture,200,100,200);*/
				g.fillRect(100*voiture,200,100,200);
				
				g.setColor(Color.RED);
				g.drawString("Bus",50+28*5+5,250-10*5-2 );
				/*g.drawRect(50+28*bus,250-10*5,25,10*5);*/
				g.fillRect(100*bus,200,300,200);
				
				g.setColor(Color.RED);
				g.drawString("Bicyclette",50+28*5+5,250-10*5-2 );
				/*g.drawRect(50+28*bicyclette,250-10*5,25,10*5);*/
				g.fillRect(100*bicyclette,200,500,200);
				
				g.setColor(Color.RED);
				g.drawString("Pieton",50+28*5+5,250-10*5-2 );
				/*g.drawRect(50+28*pieton,250-10*5,25,10*5);*/
				g.fillRect(100*pieton,200,800,200);
					
				
				
					
			}
		};
		dessin.setPreferredSize(new Dimension(600, 480));
		fenetre.setContentPane(dessin);
		fenetre.pack();
		fenetre.setVisible(true);
	}
}
