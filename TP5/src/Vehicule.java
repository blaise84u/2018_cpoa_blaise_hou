
public abstract class Vehicule {

	private double vitesse;
	protected double vitesseMax;
	protected String type;
	
	public Vehicule(double vmax,String t) {
		this.vitesseMax=vmax;
		this.type=t;
		this.vitesse=0;

	}

	public double getVitesse() {
		return vitesse;
	}

	public String getType() {
		return type;
	}
	
	public void accelerer(double v) {
		if(vitesseMax>this.vitesse+v) {
			this.vitesse=this.vitesse+v;
		}else {
			this.vitesse=this.vitesseMax;
			}
	}
	
	public void decelerer(double v) {
		if(0<this.vitesse-v) {
			this.vitesse=this.vitesse-v;
		}else {
			this.vitesse=0;
			}
	}
}
