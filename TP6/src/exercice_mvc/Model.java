package exercice_mvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

public class Model extends Observable {

		
		private List<String> liste;
		
		public Model(){
			this.liste=new ArrayList<String>();
		}
		
	
		public void modifier(String s){
			this.liste.add(s);
			
			this.setChanged();		//Informe du changement
			this.notifyObservers(); //Appel de update sur toutes les vues
		}

		/**
		 * @return the compteur
		 */
		public List<String> getListe() {
			
			System.out.println(this.liste);
			return this.liste;
		}
		

}


