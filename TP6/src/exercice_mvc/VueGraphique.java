package exercice_mvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class VueGraphique extends JPanel implements Observer {
	
	private List<String> l;
	private JTextField saisie,max,min; 
	private Controler control;
	private Model model;
	
	public VueGraphique(Controler c) {
		this.control=c;
		
		this.model=new Model();
		this.model.addObserver(this.vueg);
		
		this.setLayout(new BorderLayout());
		this.saisie= new JTextField(10);
		 saisie.setPreferredSize(new Dimension(200,30));
		 JPanel panneauDeControle= new JPanel(new GridLayout(1,2));
		 panneauDeControle.add(new JLabel("Donner une cha�ne "+"    ",JLabel.CENTER));
		 saisie.addKeyListener(this.control);
		 panneauDeControle.add(saisie);
		 
		 JTextArea j=new  JTextArea(5,5);
		 j.addKeyListener(this.control);
		 JPanel panMaxMin = new JPanel(new GridLayout(2,2));
		 panMaxMin.add(new JLabel("Plus grand mot ",JLabel.CENTER));
		 max= new JTextField(10);
		 max.setPreferredSize(new Dimension(200,30));
		 panMaxMin.add(max);
		 
		 panMaxMin.add(new JLabel("Plus petit mot ", JLabel.CENTER));
		 min= new JTextField(10);
		 panMaxMin.add(min);
		 
		 this.add(panneauDeControle,BorderLayout.NORTH);
		 this.add(new JScrollPane(j),BorderLayout.CENTER);
		 this.add(panMaxMin,BorderLayout.SOUTH);
		 this.setSize(new Dimension(400,400));
		 //Construction de l'IG dans une JFrame		 
		
	}
	
	public void update(Observable o, Object arg) {
		System.out.println("test32");
		this.l=((Model)o).getListe();
		String s="";
		for(int i=0;i<l.size();i++) {
			s=s+l.get(i)+'\n';
		}
		//this.setText(s);
		
	}
	public String getL(){
		return this.l.get(this.l.size()-1);
	}
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
	}
}
