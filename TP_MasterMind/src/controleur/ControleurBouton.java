package controleur;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import modele.Modele;

public class ControleurBouton implements ActionListener {
	
	private Modele m;
	
	public ControleurBouton(Modele m) {
		this.m=m;
	}
	
	
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Masquer")){
			if(m.getMasquer()){
				m.setMasquer(false);
			}else {
				m.setMasquer(true);
			}
	
		}
			
		if(e.getActionCommand().equals("Valider")){
			m.valider();
		}
				
	}

}
