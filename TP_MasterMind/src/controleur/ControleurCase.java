package controleur;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import modele.Modele;

public class ControleurCase implements MouseListener {
	
	private Modele m;
	public ControleurCase(Modele m) {
		this.m=m;
	}


	@Override
	public void mouseClicked(MouseEvent e) {
		System.out.println(e);
		int XenCours=e.getX()/50;
		m.setColonneEnCours(XenCours);
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
