package modele;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import tp_note_2017.MyImage;
import tp_note_2017.Rangee;
import tp_note_2017.RangeeSimple;

public class Modele extends Observable {
	
	protected RangeeSimple secret;
	
	protected List<Rangee> rangees;
	
	protected int ligneEnCours;
	
	protected int ColonneEnCours;
	
	protected boolean masquer;
	
	public Modele() {
		// On ajoute la combinaison de couleurs tir�e au hasard � d�couvrir 
		// en haut du JPanel.
		int[] intSecret = { (int) (Math.random() * 6 + 1), (int) (Math.random() * 6 + 1), (int) (Math.random() * 6 + 1),
				(int) (Math.random() * 6 + 1) };
		secret = new RangeeSimple(intSecret, this);
		
		this.rangees=new ArrayList<Rangee>();
		 
		for (int i = 0; i < 8; i++) {
			int[] ints = { 0, 0, 0, 0 };
			rangees.add(new RangeeSimple(ints, this));
		}
	
		this.ligneEnCours=0;
		this.ColonneEnCours=0;
		}
	


	public void selectCouleur(int couleur) {
		this.rangees.get(ligneEnCours).getInt()[ColonneEnCours]=couleur;
		if(this.ColonneEnCours>2) {
			ColonneEnCours=3;
		}else {
			ColonneEnCours++;
		}
		setChanged();
		notifyObservers();
	}
	
	public void valider() {
		ligneEnCours++;
		ColonneEnCours=00;
		setChanged();
		notifyObservers();
			
		
		
	}

	public boolean getMasquer() {
		return masquer;
	}

	public void setMasquer(boolean masquer) {
		this.masquer = masquer;
		setChanged();
		notifyObservers();
	}

	public RangeeSimple getSecret() {
		return secret;
	}

	public int getLigneEnCours() {
		return ligneEnCours;
	}
	
	public int getColonneEnCours() {
		return ColonneEnCours;
	}

	public void setColonneEnCours(int colonneEnCours) {
		ColonneEnCours = colonneEnCours;
		setChanged();
		notifyObservers();
	}

	public List<Rangee> getRangees() {
		return rangees;
	}
}
