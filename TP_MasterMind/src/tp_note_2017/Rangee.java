package tp_note_2017;

import modele.Modele;

public interface Rangee{
	
	
	public int[] getInt();
	public MyImage getImage(int i);
	public Modele getModel();
	
	
	
	
}