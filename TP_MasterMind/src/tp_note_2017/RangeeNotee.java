package tp_note_2017;

import java.awt.Color;

import modele.Modele;

public class RangeeNotee implements Rangee {
	
	protected Color[] couleursResultat={ Color.GRAY, Color.WHITE,Color.BLACK };
	private Rangee r;
	
	public RangeeNotee(Rangee r) {
		this.r=r;
	}
	
	@Override
	public int[] getInt() {
		return r.getInt();
	}
	@Override
	public MyImage getImage(int i) {
		return r.getImage(i);
	}
	@Override
	public Modele getModel() {
		return r.getModel();
	}
	

}
