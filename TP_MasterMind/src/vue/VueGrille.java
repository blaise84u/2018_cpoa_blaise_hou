package vue;

import java.awt.Color;
import java.awt.Graphics;
import java.util.List;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;

import modele.Modele;
import tp_note_2017.Rangee;
import tp_note_2017.RangeeSimple;

/**
 * La classe Grille permet de creer un JPanel contenant :
 * 
 * + l'affichage des objets MyImage associes a 8 instances de RangeeSimple
 * 
 * + l'affichage de la rangee a decouvrir en haut du JPanel, instance de RangeeSimple
 * 
 */
public class VueGrille extends JPanel implements Observer  {
	
	/**
	 * la taille des cases en pixels
	 */
	public static final int TAILLE = 50;
	private Modele m;
	
	public VueGrille(Modele m) {
		super();
		this.m=m;
	}


	/**
	 * La methode paintComponent se charge d'effectuer l'affichage de 8 instances 
	 * de la classe RangeeSimple
	 * ainsi que la rang�e � decouvrir en haut du composant.
	 * 
	 * Remarque importante : 
	 * Les instructions de creation des instances de RangeeSimple devront
	 * etre deplacees dans le constucteur de la classe Modele en tenant compte
	 * de ses attributs.
	 */
	public void paintComponent(Graphics g) {

		
		// dessine le fond en gris
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		
	
		List<Rangee> rangees=m.getRangees();
		for(int i=0;i< rangees.size();i++) {
			int hauteur = TAILLE*9 - (i + 1) * TAILLE;
			rangees.get(i).getImage(i).dessinerDansComposant(g, 0,hauteur);
		}

	
		if(m.getMasquer()){
			m.getSecret().getImage(-1).dessinerDansComposant(g, 0, 0);
		}
		

	}


	@Override
	public void update(Observable o, Object arg) {
		m=(Modele)o;
		repaint();
	}


}
